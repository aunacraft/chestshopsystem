package net.aunacraft.chestshopsystem.utils;

import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class ItemSerializeUtil {

    public static String itemStackToString(ItemStack stack) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            BukkitObjectOutputStream stream = new BukkitObjectOutputStream(out);
            stream.writeObject(stack);
            stream.close();
            return Base64.getEncoder().encodeToString(out.toByteArray());
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static ItemStack itemStackFromString(String string)  {
        try {
            ByteArrayInputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(string));
            BukkitObjectInputStream dataInputStream = new BukkitObjectInputStream(in);
            ItemStack stack = (ItemStack) dataInputStream.readObject();
            dataInputStream.close();
            return stack;
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return null;
    }
}