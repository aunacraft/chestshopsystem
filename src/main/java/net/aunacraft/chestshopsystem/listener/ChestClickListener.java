package net.aunacraft.chestshopsystem.listener;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.chestshopsystem.ChestShopSystem;
import net.aunacraft.chestshopsystem.chestshop.ChestShop;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.TileState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.json.simple.parser.ParseException;

import java.util.Objects;
import java.util.UUID;

public class ChestClickListener implements Listener {

    @EventHandler
    public void onChestClick(PlayerInteractEvent event) throws ParseException {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        assert block != null;
        AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getUniqueId());

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (block.getType() == Material.CHEST) {
                TileState state = (TileState) event.getClickedBlock().getState();
                if (!state.getPersistentDataContainer().isEmpty()) {


                    PersistentDataContainer container = state.getPersistentDataContainer();
                    ChestShop shop = ChestShop.fromString(container.get(new NamespacedKey(ChestShopSystem.getInstance(), "chestshop"), PersistentDataType.STRING));
                    Chest chest = (Chest) block.getState();

                    if (container.has(new NamespacedKey(ChestShopSystem.getInstance(), "chestshop"), PersistentDataType.STRING)) {
                        player.sendMessage("container is there");

                        assert shop != null;
                        if (!shop.getOwner().equals(player.getUniqueId().toString())) {
                            AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.notyourshop");
                            if (player.hasPermission("chestshop.see")) {
                                if (player.isSneaking()) {
                                    return;
                                }
                            }
                            if (!player.isSneaking()) {

                                if (chest.getInventory().contains(shop.getContent().getType(), shop.getHelperFromShopType(shop.getShopType()).getAmount())) {
                                    if (aunaPlayer.getCoins() >= shop.getHelperFromShopType(shop.getShopType()).getPrice()) {
                                        AunaAPI.getApi().removeCoins(aunaPlayer.getUuid(), shop.getHelperFromShopType(shop.getShopType()).getPrice());
                                        AunaAPI.getApi().addCoins(UUID.fromString(shop.getOwner()), shop.getHelperFromShopType(shop.getShopType()).getPrice());
                                        aunaPlayer.toBukkitPlayer().playSound(aunaPlayer.toBukkitPlayer().getLocation(), Sound.BLOCK_CORAL_BLOCK_FALL, 1, 0.5F);
                                        aunaPlayer.sendActionbar(AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "chestshopsystem.buyedactionbar",
                                                shop.getHelperFromShopType(shop.getShopType()).getAmount(),
                                                shop.getContent().toString().substring(0, 1).toUpperCase() + shop.getContent().toString().substring(1).toLowerCase(),
                                                shop.getHelperFromShopType(shop.getShopType()).getPrice()));
                                        for (int i = 0; i < shop.getHelperFromShopType(shop.getShopType()).getAmount(); i++) {
                                            chest.getInventory().removeItem(new ItemStack(shop.getContent().getType()));
                                            player.getInventory().addItem(new ItemStack(shop.getContent().getType()));
                                        }
                                    }
                                } else {
                                    aunaPlayer.sendActionbar(AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "chestshopsystem.shopisempty"));
                                    aunaPlayer.toBukkitPlayer().playSound(aunaPlayer.toBukkitPlayer().getLocation(), Sound.ENTITY_BLAZE_DEATH, 0.25F, 2);
                                }
                                event.setCancelled(true);
                            }
                        }
                    }
                }
            }
        }
    }
}