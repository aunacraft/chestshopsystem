package net.aunacraft.chestshopsystem.listener;

import net.aunacraft.chestshopsystem.ChestShopSystem;
import org.bukkit.NamespacedKey;
import org.bukkit.block.TileState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class InChestClickListener implements Listener {


    @EventHandler
    public void InInvClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getClickedInventory() == null) return;

        if (event.getClickedInventory().getType().equals(InventoryType.CHEST)) {
            if (event.getClickedInventory().getHolder() == null) return;
            if (event.getClickedInventory().getHolder().getInventory().getLocation() == null) return;
            if (!(event.getClickedInventory().getHolder().getInventory().getLocation().getBlock().getState() instanceof TileState)) {
                return;
            }

            TileState state = (TileState) event.getClickedInventory().getHolder().getInventory().getLocation().getBlock().getState();
            PersistentDataContainer container = state.getPersistentDataContainer();
            if (container.has(new NamespacedKey(ChestShopSystem.getInstance(), "chestshop"), PersistentDataType.BYTE)) {
                String owneruuid = container.get(new NamespacedKey(ChestShopSystem.getInstance(), "owner"), PersistentDataType.STRING);
                if (player.hasPermission("chestshop.ignoreperms")) {
                    return;
                }
                event.setCancelled(true);


            }
        }
    }
}