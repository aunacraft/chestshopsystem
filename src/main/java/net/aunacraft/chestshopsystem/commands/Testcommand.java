package net.aunacraft.chestshopsystem.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class Testcommand implements CommandExecutor, TabExecutor {

    public Testcommand(PluginCommand command) {
        command.setExecutor(this);
        command.setTabCompleter(this);
    }


    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        AunaPlayer aunaPlayer = (AunaAPI.getApi().getPlayer(((Player) sender).getUniqueId()));
        Block targetBlock = aunaPlayer.toBukkitPlayer().getTargetBlockExact(5);


        final ArmorStand armorStand = (ArmorStand) targetBlock.getWorld().spawnEntity(targetBlock.getLocation().add(0.5, -1, +0.5), EntityType.ARMOR_STAND);

        armorStand.setVisible(true);
        armorStand.setInvulnerable(false);
        armorStand.getEquipment().setHelmet(new ItemStack(Material.GILDED_BLACKSTONE));
        armorStand.setGravity(false);


        return false;
    }

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }


    private float toDegree(double angle) {
        return (float) Math.toDegrees(angle);
    }


    private Vector getVector(Entity entity) {
        if (entity instanceof Player) {
            return ((Player) entity).getEyeLocation().toVector();
        } else {
            return entity.getLocation().toVector();
        }
    }


}
