package net.aunacraft.chestshopsystem.commands;


import com.google.common.collect.Lists;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.signmenu.SignMenuFactory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.validator.AunaValidators;
import net.aunacraft.chestshopsystem.ChestShopSystem;
import net.aunacraft.chestshopsystem.chestshop.ChestShop;
import net.aunacraft.chestshopsystem.chestshop.ShopType;
import net.aunacraft.chestshopsystem.chestshop.session.ChestShopCreateSession;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.TileState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ShopSetupCommand implements CommandExecutor, TabCompleter {
    public ShopSetupCommand(PluginCommand command) {
        command.setExecutor(this);
        command.setTabCompleter(this);
    }


    // /shop <Buy | Sell>


    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        AunaPlayer aunaPlayer = (AunaAPI.getApi().getPlayer(((Player) sender).getUniqueId()));
        String language = aunaPlayer.getMessageLanguage().toString();


        if (sender.hasPermission("chestshop.create")) {
            Player player = ((Player) sender);
            Block targetBlock = player.getTargetBlock(null, 3);
            Chest chest = (Chest) targetBlock.getState();
            TileState state = (TileState) targetBlock.getState();
            ItemStack[] stacks = chest.getInventory().getContents();
            PersistentDataContainer container = state.getPersistentDataContainer();


            if (args[0].equals("verkaufen") || args[0].equals("sell") || args[0].equals("kaufen") || args[0].equals("buy")) {


                if (targetBlock.isEmpty()) {
                    sender.sendMessage("noch näher ran");
                }

                if (!targetBlock.getType().equals(Material.CHEST)) {
                    if (language.equalsIgnoreCase("GERMAN")) {
                        sender.sendMessage("Bitte schaue eine Kisten an.");
                    } else {
                        sender.sendMessage("Please look at a Chest.");
                    }
                    return true;
                }

                if (chest.getInventory().isEmpty()) {
                    player.sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.createchestempty"));
                    return true;
                }

                if (state.getPersistentDataContainer().has(new NamespacedKey(ChestShopSystem.getInstance(), "chestshop"), PersistentDataType.STRING)) {
                    player.sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.alreadyshop"));
                    return true;
                }


                if (args.length == 1) {

                    AtomicInteger amount = new AtomicInteger();
                    AtomicInteger price = new AtomicInteger();

                    SignMenuFactory
                            .newMenu( //create menu
                                    "", //line 1
                                    "--------------", //line 2
                                    AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.enteramountline1"), //line 3
                                    AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.enteramountline2"))
                            .response((amountresponder, amountLines) -> {//response
                                String numberAsString = amountLines[0];
                                if (AunaValidators.POSITIVE_INT_VALIDATOR.isValid(numberAsString)) {

                                    int amountnumber = Integer.parseInt(numberAsString);

                                    amount.set(amountnumber);

                                    SignMenuFactory
                                            .newMenu( //create menu
                                                    "", //line 1
                                                    "--------------", //line 2
                                                    AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.howmuchcoins"), //line 3
                                                    "--------------")
                                            .response((howmuchresponder, howmuchlines) -> { //response
                                                String howmuchline = howmuchlines[0];
                                                if (AunaValidators.POSITIVE_INT_VALIDATOR.isValid(howmuchline)) {

                                                    int howmuchint = Integer.parseInt(howmuchline);

                                                    price.set(howmuchint);

                                                    ItemStack found = null;
                                                    for (ItemStack stack : stacks
                                                    ) {
                                                        if (stack != null) {
                                                            found = stack;
                                                            break;
                                                        }
                                                    }

                                                    ItemStack finalFound = found;

                                                    Bukkit.getScheduler().scheduleSyncDelayedTask(ChestShopSystem.getInstance(), () -> {
                                                        ChestShopCreateSession session = new ChestShopCreateSession(aunaPlayer);
                                                        ChestShop shop = session.createShop((args[0].equals("verkaufen") || args[0].equals("sell") ? ShopType.SELL : ShopType.BUY),
                                                                price.get(), amount.get(), finalFound, targetBlock.getLocation());
                                                        System.out.println(shop.toString());
                                                        container.set(new NamespacedKey(ChestShopSystem.getInstance(), "chestshop"), PersistentDataType.STRING, shop.toString());
                                                        state.update();

                                                    });


                                                    return true;
                                                } else {
                                                    howmuchresponder.sendMessage("§cEnter a valid number");
                                                    return false;
                                                }
                                            }).open(player); //open the sign menu for player
                                    return true;
                                } else {
                                    amountresponder.sendMessage("§cEnter a valid number");
                                    return false;
                                }
                            })
                            .open(player); //open the sign menu for player
                    return false;
                } else {
                    if (language.equalsIgnoreCase("GERMAN")) {
                        sender.sendMessage("§7Use: §e/shop <erstellen oder verkaufen>");
                    } else {
                        sender.sendMessage("§7Use: §e/shop <create or sell>");
                    }
                }


            } else {
                // TODO Handle false command usage
            }
            /*if (args[0].equals("kaufen") || args[0].equals("buy")) {

                if (targetBlock.isEmpty()) {
                    sender.sendMessage("noch näher ran");
                }


                if (!targetBlock.getType().equals(Material.CHEST)) {
                    if (language.equalsIgnoreCase("GERMAN")) {
                        sender.sendMessage("Bitte schaue eine Kisten an.");
                    } else {
                        sender.sendMessage("Please look at a Chest.");
                    }
                    return true;
                }
                if (chest.getInventory().isEmpty()) {
                    player.sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.createchestempty"));
                    return true;
                }
                AtomicReference<Boolean> amount = new AtomicReference<>(false);
                AtomicReference<Boolean> howmuch = new AtomicReference<>(false);
                SignMenuFactory
                        .newMenu( //create menu
                                "", //line 1
                                "--------------", //line 2
                                AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.enteramountline1"), //line 3
                                AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.enteramountline2"))
                        .response((amountresponder, amountLines) -> {//response
                            String numberAsString = amountLines[0];
                            if (AunaValidators.POSITIVE_INT_VALIDATOR.isValid(numberAsString)) {
                                amount.set(true);
                                int amountnumber = Integer.parseInt(numberAsString);


                                SignMenuFactory
                                        .newMenu( //create menu
                                                "", //line 1
                                                "--------------", //line 2
                                                AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "chestshopsystem.howmuchcoins"), //line 3
                                                "--------------")
                                        .response((howmuchresponder, howmuchlines) -> { //response
                                            String howmuchline = howmuchlines[0];
                                            if (AunaValidators.POSITIVE_INT_VALIDATOR.isValid(howmuchline)) {
                                                howmuch.set(true);
                                                int howmuchint = Integer.parseInt(howmuchline);


                                                ItemStack found = null;
                                                for (ItemStack stack : stacks
                                                ) {
                                                    if (stack != null) {
                                                        found = stack;
                                                        break;
                                                    }
                                                }

                                                ItemStack finalFound = found;

                                                Bukkit.getScheduler().scheduleSyncDelayedTask(ChestShopSystem.getInstance(), () -> {
                                                    container.set(new NamespacedKey(ChestShopSystem.getInstance(), "chestshop"), PersistentDataType.BYTE, (byte) 0);
                                                    container.set(new NamespacedKey(ChestShopSystem.getInstance(), "type"), PersistentDataType.STRING, "buy");
                                                    container.set(new NamespacedKey(ChestShopSystem.getInstance(), "owner"), PersistentDataType.STRING, aunaPlayer.toBukkitPlayer().getUniqueId().toString());
                                                    container.set(new NamespacedKey(ChestShopSystem.getInstance(), "amount"), PersistentDataType.INTEGER, amountnumber);
                                                    container.set(new NamespacedKey(ChestShopSystem.getInstance(), "howmuch"), PersistentDataType.INTEGER, howmuchint);
                                                    container.set(new NamespacedKey(ChestShopSystem.getInstance(), "items"), PersistentDataType.STRING, finalFound.getType().name());
                                                    state.update();


                                                    ArmorStand armorStand = (ArmorStand) targetBlock.getWorld().spawnEntity(targetBlock.getLocation().add(0.5, -1, +0.5), EntityType.ARMOR_STAND);
                                                    //armorStand.setVisible(false);
                                                    //armorStand.setInvulnerable(true);
                                                    armorStand.setHelmet(new ItemStack(Material.matchMaterial(finalFound.toString())));
                                                    armorStand.setGravity(false);


                                                });


                                                return true;
                                            } else {
                                                howmuchresponder.sendMessage("§cEnter a valid number");
                                                return false;
                                            }
                                        }).open(player); //open the sign menu for player


                                return true;
                            } else {
                                amountresponder.sendMessage("§cEnter a valid number");
                                return false;
                            }

                        })
                        .open(player); //open the sign menu for player


                return false;

            }*/


            return false;
        }
        return true;
    }


    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player && sender.hasPermission("chestshop.create") && args.length == 1) {
            List<String> matches = Lists.newArrayList();
            matches.add("verkaufen");
            matches.add("sell");
            matches.add("kaufen");
            matches.add("buy");
            Collections.sort(matches);
            return matches;
        }
        return null;
    }
}