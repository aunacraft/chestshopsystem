package net.aunacraft.chestshopsystem;

import net.aunacraft.chestshopsystem.commands.ShopSetupCommand;
import net.aunacraft.chestshopsystem.commands.Testcommand;
import net.aunacraft.chestshopsystem.listener.ChestClickListener;
import net.aunacraft.chestshopsystem.listener.InChestClickListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public final class ChestShopSystem extends JavaPlugin {
    private static ChestShopSystem instance;


    public ChestShopSystem() {
        instance = this;
    }

    public static ChestShopSystem getInstance() {
        return instance;
    }


    @Override
    public void onEnable() {
        //AunaAPI.getApi().getMessageService().applyPrefix("chestshopsystem", "chestshopsystem.prefix");
        //AunaAPI.getApi().registerCommand(new ShopSetupCommand());


        new ShopSetupCommand(Objects.requireNonNull(this.getCommand("shop")));
        new Testcommand(Objects.requireNonNull(this.getCommand("testing")));
        Bukkit.getPluginManager().registerEvents(new ChestClickListener(), this);
        Bukkit.getPluginManager().registerEvents(new InChestClickListener(), this);

    }


    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}