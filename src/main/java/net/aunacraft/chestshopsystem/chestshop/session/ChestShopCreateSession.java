package net.aunacraft.chestshopsystem.chestshop.session;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.chestshopsystem.chestshop.ChestShop;
import net.aunacraft.chestshopsystem.chestshop.ShopType;
import net.aunacraft.chestshopsystem.chestshop.helper.TransactionHelper;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class ChestShopCreateSession {

    private final AunaPlayer player;

    public ChestShopCreateSession(AunaPlayer player) {
        this.player = player;
    }


    public ChestShop createShop(ShopType shopType, long price, int amount, ItemStack itemStack, Location location) {
        if(shopType.equals(ShopType.SELL)){
            ChestShop chestShop = new ChestShop(location, itemStack, shopType, this.player.getUuid().toString(), new TransactionHelper(amount, price, shopType), null);
            chestShop.spawn(() -> player.sendActionbar(AunaAPI.getApi().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "chestshopsystem.successfullcreate")));
            return chestShop;
        }else{
            ChestShop chestShop = new ChestShop(location, itemStack, shopType, this.player.getUuid().toString(), null, new TransactionHelper(amount, price, shopType));
            chestShop.spawn(() -> player.sendActionbar(AunaAPI.getApi().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "chestshopsystem.successfullcreate")));
            return chestShop;
        }
    }

}