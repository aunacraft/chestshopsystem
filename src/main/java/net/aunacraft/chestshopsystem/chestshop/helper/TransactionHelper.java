package net.aunacraft.chestshopsystem.chestshop.helper;

import com.google.gson.JsonObject;
import lombok.Getter;
import lombok.Setter;
import net.aunacraft.chestshopsystem.chestshop.ShopType;
import org.json.simple.JSONValue;

@Getter
@Setter
public class TransactionHelper {

    private int amount;
    private long price;
    private ShopType self;

    public TransactionHelper(int amount, long price, ShopType self) {
        this.amount = amount;
        this.price = price;
        this.self = self;
    }

    @Override
    public String toString() {
        return "TransactionHelper{" +
                "amount=" + amount +
                ", price=" + price +
                ", self=" + self +
                '}';
    }

    public static TransactionHelper fromString(String string){
        final JsonObject jsonObject = (JsonObject) JSONValue.parse(string);
        return new TransactionHelper(jsonObject.get("amount").getAsInt(), jsonObject.get("price").getAsLong(), ShopType.valueOf(jsonObject.get("self").getAsString()));
    }
}
