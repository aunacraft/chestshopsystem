package net.aunacraft.chestshopsystem.chestshop;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import lombok.Getter;
import lombok.Setter;
import net.aunacraft.api.bukkit.util.LocationUtil;
import net.aunacraft.chestshopsystem.chestshop.helper.TransactionHelper;
import net.aunacraft.chestshopsystem.utils.ItemSerializeUtil;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.Reader;

@Getter
@Setter
public class ChestShop {

    public static final int SELL_INDEX = 0,
            BUY_INDEX = 1;
    private final Location location;
    private final String content;
    private ShopType shopType;
    private final String owner;
    private TransactionHelper helper1;
    private TransactionHelper helper2;

    public ChestShop(final Location location, final ItemStack content, final ShopType shopType, final String owner, final TransactionHelper buy, final TransactionHelper sell) {
        this.location = location;
        this.shopType = shopType;
        this.content = ItemSerializeUtil.itemStackToString(content);
        this.owner = owner;
        this.helper1 = buy;
        this.helper2 = sell;
    }

    public ItemStack getContent() {
        return ItemSerializeUtil.itemStackFromString(this.content);
    }

    @Nullable
    public static ChestShop fromString(final String jsonContext) throws ParseException {

        JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonContext);

        if(jsonObject == null){
            throw new NullPointerException("jsonObjectIsNull");
        }
        if(jsonObject.get("shopType") == null){
            throw new NullPointerException("shopTypeIsNull");
        }
        TransactionHelper[] array = new TransactionHelper[(
                ShopType.valueOf(
                        (String) jsonObject.get("shopType"))
                        .equals(
                                ShopType.BOTH) ? 2 : 1)];
        if (array.length == 1) {
            if (!((String) jsonObject.get("transactionHelper1")).equalsIgnoreCase("null")) {
                array[0] = (TransactionHelper.fromString((String) jsonObject.get("transactionHelper1")));
            } else {
                array[1] = TransactionHelper.fromString((String) jsonObject.get("transactionHelper2"));
            }
        } else {
            array[0] = TransactionHelper.fromString((String) jsonObject.get("transactionHelper1"));
            array[1] = TransactionHelper.fromString((String) jsonObject.get("transactionHelper2"));
        }
        ChestShop chestShop = new ChestShop(LocationUtil.locationFromString(jsonObject.get("location").toString()),
                ItemSerializeUtil.itemStackFromString(jsonObject.get("content").toString()),
                ShopType.valueOf(jsonObject.get("shopType").toString()),
                jsonObject.get("owner").toString(),
                array[0],
                array[1]
                );
        return chestShop;
    }

    public void spawn(Runnable callback) {
        Block targetBlock = this.location.getBlock();
        final ArmorStand armorStand = (ArmorStand) targetBlock.getWorld().spawnEntity(targetBlock.getLocation().add(0.5, -1, +0.5), EntityType.ARMOR_STAND);
        armorStand.setVisible(false);
        armorStand.setInvulnerable(true);
        armorStand.getEquipment().setHelmet(new ItemStack(this.getContent().getType()));
        armorStand.setGravity(false);
        callback.run();
    }

    public TransactionHelper getHelperFromShopType(final ShopType shopType) {
        switch (shopType){
            case SELL:
                return this.helper1;
            case BUY:
                return this.helper2;
            default:
                throw new NullPointerException("No existant Value");
        }
    }

    /*@Override
    public String toString() {
        JSONArray array = new JSONArray();

        JSONObject object = new JSONObject();
        object.put("location", LocationUtil.locationToString(this.location));
        array.add(object.toString());

        JSONObject object1 = new JSONObject();
        object1.put("shoptype", this.shopType);
        object1.put("itemstack", this.content);
        object1.put("onwer", this.owner);
        array.add(object1);

        JSONObject object2 = new JSONObject();
        object2.put("helper1", (this.transactionHelpers[SELL_INDEX] != null ? this.transactionHelpers[SELL_INDEX].toString() : "null"));
        object2.put("helper2", (this.transactionHelpers[BUY_INDEX] != null ? this.transactionHelpers[BUY_INDEX].toString() : "null"));
        array.add(object2);

        return array.toString();
    }*/

    @Override
    public String toString() {
        return "ChestShop{" +
                "location=" + LocationUtil.locationToString(this.location) +
                ", content='" + content + '\'' +
                ", shopType=" + shopType +
                ", owner='" + owner + '\'' +
                ", transactionHelper1='" + (helper1 != null ? helper1.toString() : "null") + '\'' +
                ", transactionHelper2='" + (helper2 != null ? helper2.toString() : "null") + '\'' +
                '}';
    }
}